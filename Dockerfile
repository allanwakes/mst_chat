FROM python:3.8-slim-buster

ENV WORKDIR=/usr/src/app
ENV USER=app
ENV APP_HOME=/web
ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1

WORKDIR $WORKDIR
RUN apt update && apt install gcc libpq-dev -y

RUN pip install --upgrade pip
COPY ./requirements.txt $WORKDIR/requirements.txt
RUN pip install -r requirements.txt

RUN adduser --system --group $USER
RUN mkdir $APP_HOME
# WORKDIR $APP_HOME

# COPY . $APP_HOME
RUN chown -R $USER:$USER $APP_HOME
USER $USER