from pydantic import BaseModel, Field, validator
from enum import Enum
import re
from better_profanity import profanity


class CharTypeEnum(str, Enum):
    monster = 'Monster'
    hero = 'Hero'


class NameEnum(str, Enum):
    aberration = "Aberration"
    animal = "Animal"
    construct = "Construct"
    dragon = "Dragon"
    elemental = "Elemental"
    fey = "Fey"
    giant = "Giant"
    humanoid = "Humanoid"
    magical_beast = "Magical Beast"
    monstrous_humanoid = "Monstrous Humanoid"
    ooze = "Ooze"
    outsider = "Outsider"
    plant = "Plant"
    undead = "Undead"
    vermin = "Vermin"
    barbarian = "Barbarian"
    bard = "Bard"
    cleric = "Cleric"
    druid = "Druid"
    fighter = "Fighter"
    monk = "Monk"
    paladin = "Paladin"
    ranger = "Ranger"
    rogue = "Rogue"
    sorcerer = "Sorcerer"
    wizard = "Wizard"


class ChatMessage(BaseModel):
    name: NameEnum
    token_id: int = Field(..., gt=0)
    content: str
    char_type: CharTypeEnum

    address: str
    signature: str
    payload: str

    @validator('content')
    def no_chinese(cls, v):
        # remove space from head and tail
        v = v.strip()
        # length check
        assert 0 < len(v) <= 140, 'content length within 1-140'
        assert v.isascii(), 'ascii char only'
        # content censor
        # v = profanity.censor(v, '-')  # 总是----
        # remove Chinese
        # return re.sub(r'[\u4e00-\u9fff]+', '----', v)  # 和敏感词保持统一
        return profanity.censor(v, '-')
