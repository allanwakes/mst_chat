from pydantic import BaseModel, Field
from enum import Enum, IntEnum
from typing import Optional


class ActionEnum(str, Enum):
    copper_ranking = 'mining'
    win_ranking = 'battle'
    # for bot
    bot_mined = 'mined'
    bot_ratio = 'ratio'
    # for personal review
    my_events = 'events'
    my_stat = 'stat'


class CharTypeEnum(IntEnum):
    hero = 1
    monster_1 = 2
    monster_2 = 3


class EncounterOneMessage(BaseModel):
    action: ActionEnum
    char_type: CharTypeEnum
    char_id: Optional[int] = Field(1, gt=0, description="The char id must be greater than zero")