from pydantic import BaseModel, validator
from enum import Enum


class CategoryEnum(str, Enum):
    egg = 'egg'
    monster_genesis = 'm1'
    monster_reborn = 'm2'
    summoner = 'hero'


class MarketPersonalDealsMessage(BaseModel):
    category: CategoryEnum
    seller: str

    @validator('seller')
    def address_check(cls, v):
        if len(v) != 42 or not v.startswith('0x'):
            raise ValueError('address length invalid')
        
        return v.lower()