from dynaconf import Dynaconf


settings = Dynaconf(
    settings_files=['app/settings.toml', '.secrets.toml'],
    environments=True,
)