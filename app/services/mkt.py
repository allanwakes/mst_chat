from app.config.config import settings
import psycopg2.extras


per_done_orders_q = f"select char_id, deal_price, buyer from {settings.table.mkt.orders} where seller = %s and category = %s and status = 2"
b_asks_q = f"select count(id) as no, category from {settings.table.mkt.orders} where status = 1 group by category"
b_24deal_no_q = f"""
select category, COALESCE(res.no, 0) as no from (values ('egg'), ('m1'), ('m2'), ('hero')) v (category) 
left join
(
    select count(id) as no, category from {settings.table.mkt.orders} 
    where status = 2 and updated_at >= NOW() - INTERVAL '24 HOURS' group by category
) as res using (category)"""
b_24deal_v_q = f"""
select category, COALESCE(res.no, 0) as no from (values ('egg'), ('m1'), ('m2'), ('hero')) v (category) 
left join
(
    select sum(list_price) as no, category from {settings.table.mkt.orders} 
    where status = 2 and updated_at >= NOW() - INTERVAL '24 HOURS' group by category
) as res using (category)"""
b_floor_q = f"select min(list_price) as no, category from {settings.table.mkt.orders} where status = 1 group by category"


def get_personal_done_orders(pool, seller: str, category: str):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(per_done_orders_q, (seller, category))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_mkt_brief(pool):
    result = dict()

    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cursor.execute(b_asks_q)
    records = cursor.fetchall()
    result["ask_stats"] = records

    cursor.execute(b_24deal_no_q)
    records = cursor.fetchall()
    result["deal_no24"] = records

    cursor.execute(b_24deal_v_q)
    records = cursor.fetchall()
    result["deal_value24"] = records

    cursor.execute(b_floor_q)
    records = cursor.fetchall()
    result["floor"] = records

    cursor.close()
    pool.putconn(conn)

    return result