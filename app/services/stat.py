from app.config.config import settings
import psycopg2.extras


emf1_copper_query = f"select char_id, copper from {settings.table.emf1.stats} where char_type = %s order by copper desc, char_id asc limit 50"
emf1_battle_query = f"select char_id, win from {settings.table.emf1.stats} where char_type = %s order by win desc, char_id asc limit 50"
emf2_copper_query = f"select char_id, copper from {settings.table.emf2.stats} where char_type = %s order by copper desc, char_id asc limit 50"
emf2_battle_query = f"select char_id, win_battle as win from {settings.table.emf2.stats} where char_type = %s order by win_battle desc, char_id asc limit 50"
emf1_copper_mined_q = f"select sum(copper) from {settings.table.emf1.stats} where char_type = %s"
emf1_winning_ratio_q = f"SELECT ROUND(SUM(win)/SUM(battle)::numeric, 4) FROM {settings.table.emf1.stats} WHERE char_type = %s"
emf2_copper_mined_q = f"select sum(copper) from {settings.table.emf2.stats} where char_type = %s"
emf2_winning_ratio_q = f"SELECT ROUND(SUM(win_battle)/SUM(battle)::numeric, 4) FROM {settings.table.emf2.stats} WHERE char_type = %s"
emf3_copper_query = f"select char_id, copper_away as copper from {settings.table.emf3.stats} where char_type = %s order by copper_away desc, char_id asc limit 50"
emf3_battle_query = f"select char_id, win from {settings.table.emf3.stats} where char_type = %s order by win desc, char_id asc limit 50"

emf1_char_stat = f"select enter, copper, battle, win from {settings.table.emf1.stats} where char_type = %s and char_id = %s"
emf1_m_en = f"select monster_win, amount from {settings.table.emf1.events} where monster_id = %s and event_type = 'Encountered'"
emf1_h_en = f"select monster_win, amount from {settings.table.emf1.events} where hero_id = %s and event_type = 'Encountered'"
emf2_char_stat = f"select enter, flee, dropped, copper, battle, win_battle, bw_amount, fightback, win_fightback, fbw_amount, pick, pick_amount from {settings.table.emf2.stats} where char_type = %s and char_id = %s"
emf3_char_stat = f"select enter, battle_started, battle_involved, win, lose, copper_win, copper_lose, copper_away, max_battle_started from {settings.table.emf3.stats} where char_type = %s and char_id = %s"


def get_emf1_ranking_copper(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf1_copper_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf2_ranking_copper(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf2_copper_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf3_ranking_copper(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf3_copper_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf1_ranking_battle(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf1_battle_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf2_ranking_battle(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf2_battle_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf3_ranking_battle(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf3_battle_query, (char_type, ))
    records = cursor.fetchall()

    cursor.close()
    pool.putconn(conn)

    return records


def get_emf1_copper_mined(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor()
    cursor.execute(emf1_copper_mined_q, (char_type, ))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record[0]


def get_emf2_copper_mined(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor()
    cursor.execute(emf2_copper_mined_q, (char_type, ))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record[0]


def get_emf1_winning_ratio(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor()
    cursor.execute(emf1_winning_ratio_q, (char_type, ))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record[0]


def get_emf2_winning_ratio(pool, char_type: int):
    conn = pool.getconn()
    cursor = conn.cursor()
    cursor.execute(emf2_winning_ratio_q, (char_type, ))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record[0]


def get_emf1_personal_data(pool, char_type: int, char_id: int):
    if char_type == 2:
        return None
    
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf1_char_stat, (char_type, char_id))
    record = cursor.fetchone()
    if record == None:
        cursor.close()
        pool.putconn(conn)
        
        return None

    if char_type == 3:
        cursor.execute(emf1_m_en, (char_id, ))
        rs = cursor.fetchall()
        copper_won_combat = sum([r["amount"] for r in rs if r["monster_win"]])
        copper_lost_combat = sum([r["amount"] for r in rs if not r["monster_win"]])
    else:
        cursor.execute(emf1_h_en, (char_id, ))
        rs = cursor.fetchall()
        copper_won_combat = sum([r["amount"] for r in rs if not r["monster_win"]])
        copper_lost_combat = sum([r["amount"] for r in rs if r["monster_win"]])

    cursor.close()
    pool.putconn(conn)

    record["copper_won_combat"] = copper_won_combat
    record["copper_lost_combat"] = copper_lost_combat
    
    return record


def get_emf2_personal_data(pool, char_type: int, char_id: int):
    if char_type == 2:
        return None
    
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf2_char_stat, (char_type, char_id))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record


def get_emf3_personal_data(pool, char_type: int, char_id: int):
    if char_type == 2:
        return None
    
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(emf3_char_stat, (char_type, char_id))
    record = cursor.fetchone()

    cursor.close()
    pool.putconn(conn)

    return record


def get_all_emf_ranking_copper(pool, char_type: int, char_id: int = 0):
    final_res = {}
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cursor.execute(emf1_copper_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf1"] = records
    cursor.execute(emf2_copper_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf2"] = records
    cursor.execute(emf3_copper_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf3"] = records

    cursor.close()
    pool.putconn(conn)

    return final_res


def get_all_emf_ranking_battle(pool, char_type: int, char_id: int = 0):
    final_res = {}
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cursor.execute(emf1_battle_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf1"] = records
    cursor.execute(emf2_battle_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf2"] = records
    cursor.execute(emf3_battle_query, (char_type,))
    records = cursor.fetchall()
    final_res["emf3"] = records

    cursor.close()
    pool.putconn(conn)

    return final_res


def get_all_emf_personal_data(pool, char_type: int, char_id: int):
    if char_type == 2:
        return None

    final_res = {}
    conn = pool.getconn()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cursor.execute(emf1_char_stat, (char_type, char_id))
    record = cursor.fetchone()
    final_res["emf1"] = record
    cursor.execute(emf2_char_stat, (char_type, char_id))
    record = cursor.fetchone()
    final_res["emf2"] = record
    cursor.execute(emf3_char_stat, (char_type, char_id))
    record = cursor.fetchone()
    final_res["emf3"] = record

    cursor.close()
    pool.putconn(conn)

    return final_res
