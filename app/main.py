from fastapi import FastAPI
from app.config.config import settings
from app.routers import chat
from app.routers import stat
from app.routers import mkt
from app.routers import misc
from fastapi.exceptions import RequestValidationError
from app.utils.request_exceptions import request_validation_exception_handler
# from fastapi.middleware.cors import CORSMiddleware
import psycopg2
from psycopg2 import pool
import redis
from starlette.middleware.cors import CORSMiddleware


def create_app() -> CORSMiddleware:
    fastapi_app = FastAPI(title=settings.app.title, version=settings.app.version, description=settings.app.desc)

    @fastapi_app.exception_handler(RequestValidationError)
    async def custom_validation_exception_handler(request, e):
        return await request_validation_exception_handler(request, e)

    @fastapi_app.on_event("startup")
    def startup():
        fastapi_app.state.db_pool = psycopg2.pool.ThreadedConnectionPool(
            settings.db.min_pool_size, settings.db.max_pool_size, 
            database=settings.db.name, user=settings.db.user, password=settings.db.password, host=settings.db.host, port=settings.db.port)
        fastapi_app.state.redis = redis.Redis(
            host=settings.redis.host, port=settings.redis.port, 
            username=settings.redis.user, password=settings.redis.password, decode_responses=True)

    @fastapi_app.on_event("shutdown")
    async def shutdown():
        fastapi_app.state.db_pool.closeall()
        fastapi_app.state.redis.close()

    fastapi_app.include_router(chat.router)
    fastapi_app.include_router(stat.router)
    fastapi_app.include_router(mkt.router)
    fastapi_app.include_router(misc.router)

    return CORSMiddleware(fastapi_app, allow_origins=settings.cors.origins, allow_credentials=True, allow_methods=["*"], allow_headers=["*"])


app = create_app()