from fastapi import APIRouter, Request
import requests
from app.config.config import settings
import decimal
from decimal import Decimal, ROUND_DOWN


router = APIRouter(
    prefix="/misc",
    tags=["misc"]
)


@router.get("/mst")
def mst_price(request: Request):
    redis = request.app.state.redis
    res_data = redis.get(settings.redis.key.mstprice)
    if res_data is not None:
        return {"code": 200, "msg": "", "data": Decimal(res_data)}

    fura_res = requests.post(
            settings.fura.url, 
            json={
                "operationName": "pairs", "variables": {},
                "query": 'query pair {\n pair(id: \"0x1f5c5b104d6246b3d096135806cd6c6e53e206f1\") {\n name\n token0{\n derivedETH\n }\n token1{\n derivedETH\n }\n }\n }\n'
            })
    # bian_res = requests.get(settings.bian.url)
    try:
        pair = fura_res.json().get("data").get("pair")
        mst_per_ftm = pair.get("token1").get("derivedETH")
        usdc_per_ftm = pair.get("token0").get("derivedETH")
        result = (Decimal(f"{mst_per_ftm}") / Decimal(f"{usdc_per_ftm}")).quantize(Decimal('0.000000000000000000'), rounding=ROUND_DOWN)
    except Exception as e:
        print(f"PRICE_PROCESSING WITH {e}")
        return {"code": 500, "msg": "ERROR_WHEN_PROCESSING_PRICE", "data": None}

    redis.setex(settings.redis.key.mstprice, 10, str(result))
    return {"code": 200, "msg": "", "data": result}

    