from fastapi import APIRouter
from app.schemas.chat import ChatMessage
import requests
from app.config.config import settings
import binascii
from web3.auto import w3
from eth_account.messages import encode_defunct
import arrow


router = APIRouter(
    prefix="/chat",
    tags=["chat"]
)


@router.post("/message")
def message(msg: ChatMessage):
    # 校验签名，看时效
    payload = encode_defunct(text=msg.payload)
    addr = w3.eth.account.recover_message(payload, signature=binascii.unhexlify(msg.signature))
    if addr.lower() != msg.address.lower():
        return {"code": 402, "msg": "signature mismatch", "data": None}
    
    past = arrow.get(int(msg.payload))
    if arrow.utcnow() > past.shift(hours=48):
        return {"code": 402, "msg": "token no longer valid", "data": None}


    data = {
        "token_id": msg.token_id,
        "name": msg.name,
        "content": msg.content,
        "char_type": msg.char_type,
        "address": msg.address
    }

    requests.post(
        f"{settings.supa.url}/{settings.chat_table}", 
        json=data, 
        headers={"apikey": settings.supa.key, "Authorization": f"Bearer {settings.supa.key}"})

    return {"code": 201, "msg": "", "data": None}