from fastapi import APIRouter, Request
from app.services.mkt import get_personal_done_orders, get_mkt_brief
from app.schemas.mkt import MarketPersonalDealsMessage


router = APIRouter(
    prefix="/mkt",
    tags=["mkt"]
)


@router.post("/personal")
def per_query(request: Request, msg: MarketPersonalDealsMessage):
    pool = request.app.state.db_pool

    data = get_personal_done_orders(pool, msg.seller, msg.category)

    return {"code": 200, "msg": "", "data": data}


@router.get("/brief")
def brief_query(request: Request):
    pool = request.app.state.db_pool

    data = get_mkt_brief(pool)

    return {"code": 200, "msg": "", "data": data}