from fastapi import APIRouter, Request
from app.schemas.stat import EncounterOneMessage, ActionEnum
from app.services.stat import get_emf1_ranking_battle, get_emf1_ranking_copper, get_emf1_copper_mined, get_emf1_winning_ratio, get_emf1_personal_data
from app.services.stat import get_emf2_ranking_battle, get_emf2_ranking_copper, get_emf2_copper_mined, get_emf2_winning_ratio, get_emf2_personal_data
from app.services.stat import get_emf3_ranking_battle, get_emf3_ranking_copper, get_emf3_personal_data
from app.services.stat import get_all_emf_ranking_copper, get_all_emf_personal_data, get_all_emf_ranking_battle


router = APIRouter(
    prefix="/stat",
    tags=["stat"]
)


@router.post("/emf1")
def make_query(request: Request, msg: EncounterOneMessage):
    pool = request.app.state.db_pool

    if msg.action == ActionEnum.copper_ranking:
        data = get_emf1_ranking_copper(pool, msg.char_type)
    elif msg.action == ActionEnum.win_ranking:
        data = get_emf1_ranking_battle(pool, msg.char_type)
    elif msg.action == ActionEnum.bot_mined:
        data = get_emf1_copper_mined(pool, msg.char_type)
    elif msg.action == ActionEnum.bot_ratio:
        data = get_emf1_winning_ratio(pool, msg.char_type)
    elif msg.action == ActionEnum.my_stat:
        data = get_emf1_personal_data(pool, msg.char_type, msg.char_id)
    else:
        data = None

    return {"code": 200, "msg": "", "data": data}


@router.post("/emf2")
def make_emf2_query(request: Request, msg: EncounterOneMessage):
    pool = request.app.state.db_pool

    if msg.action == ActionEnum.copper_ranking:
        data = get_emf2_ranking_copper(pool, msg.char_type)
    elif msg.action == ActionEnum.win_ranking:
        data = get_emf2_ranking_battle(pool, msg.char_type)
    elif msg.action == ActionEnum.bot_mined:
        data = get_emf2_copper_mined(pool, msg.char_type)
    elif msg.action == ActionEnum.bot_ratio:
        data = get_emf2_winning_ratio(pool, msg.char_type)
    elif msg.action == ActionEnum.my_stat:
        data = get_emf2_personal_data(pool, msg.char_type, msg.char_id)
    else:
        data = None

    return {"code": 200, "msg": "", "data": data}


@router.post("/emf3")
def make_emf3_query(request: Request, msg: EncounterOneMessage):
    pool = request.app.state.db_pool

    if msg.action == ActionEnum.copper_ranking:
        data = get_emf3_ranking_copper(pool, msg.char_type)
    elif msg.action == ActionEnum.win_ranking:
        data = get_emf3_ranking_battle(pool, msg.char_type)
    elif msg.action == ActionEnum.my_stat:
        data = get_emf3_personal_data(pool, msg.char_type, msg.char_id)
    else:
        data = None

    return {"code": 200, "msg": "", "data": data}


@router.post("/emf")
def make_all_emf_query(request: Request, msg: EncounterOneMessage):
    pool = request.app.state.db_pool

    if msg.action == ActionEnum.copper_ranking:
        data = get_all_emf_ranking_copper(pool, msg.char_type)
    elif msg.action == ActionEnum.win_ranking:
        data = get_all_emf_ranking_battle(pool, msg.char_type)
    elif msg.action == ActionEnum.my_stat:
        data = get_all_emf_personal_data(pool, msg.char_type, msg.char_id)
    else:
        data = None

    return {"code": 200, "msg": "", "data": data}
